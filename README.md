#FIU Treasury Website
The project codename- for use with documentation, version control, and directory structure- is **pumpkin**.    
The site can be found at http://treasury.fiu.edu, and a working copy of the development branch is usually available at http://crow1170.com/pumpkin. The canonical repository is available at https://bitbucket.org/crow1170/pumpkin.  

##Planned Development
For future mobile versions, a hamburger menu (like [this one](http://www.jqueryscript.net/demo/jQuery-Animated-Side-Navigation-Menu-Plugin-Sidebar/)) is interesting, but not a priority.
The site would benefit from rewritten and consistent copy.
Currently, the focus is on optimization and compliance with best practices, namely unifying and confining all styling to one stylesheet, minifying all resources, and improving documentation and readability.

##Authors
The project is currently being developed by Christopher Knowles, cknow003@fiu.edu.
The styles are largely based on work provided by Jason Ford, jason@digital-thought.com.

##License
The project is protected by the MIT License. Feel free to use, modify, distribute, etc.